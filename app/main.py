import asyncio
import sys

from fastapi import FastAPI
from uvicorn import run

app = FastAPI()

@app.get("/", response_model=dict[str, str])
async def hello_world():
    return {"message": "hello world"}

if __name__ == "__main__":
    if "test" in sys.argv:
        obj = asyncio.run(hello_world())
        assert isinstance(obj, dict)
    else:
        run("main:app", host="0.0.0.0", port=5000, reload=True)
